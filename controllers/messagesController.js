const Messages = require("../models/Messages");
const catchAsync = require("../utils/catchAsync");

exports.index = catchAsync(async (req, res) => {
  const data = await Messages.find();
  res.status(200).send(data);
});

exports.store = catchAsync(async (req, res) => {
  const message = req.body;
  const data = await Messages.create({ ...message, sender_id: req.user._id, sender_username: req.user.username });
});
