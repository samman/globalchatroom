const Users = require("../models/Users");
const catchAsync = require("../utils/catchAsync");

exports.index = catchAsync(async (req, res) => {
  const users = await Users.find({ _id: { $ne: req.user._id } });
  res.status(200).json({
    status: "success",
    users,
  });
});
