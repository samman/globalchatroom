const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Users = require("../models/Users");
const AppError = require("../utils/appError");
const catchAsync = require("../utils/catchAsync");

const createJwtToken = (payload) => {
  return jwt.sign(payload, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
};

exports.signin = catchAsync(async (req, res, next) => {
  const { username, password } = req.body;
  if (!username || !password) {
    return next(new AppError(400, "Username and password are required."));
  }
  const user = await Users.findOne({ username }).select("+password");
  if (!user || !(await bcrypt.compare(password, user.password))) {
    return next(new AppError(400, "Incorrect username or password."));
  }
  const token = createJwtToken({ _id: user._id });
  res.status(200).json({
    status: "success",
    token,
  });
});

exports.signup = catchAsync(async (req, res) => {
  const user = await Users.create({
    username: req.body.username,
    password: req.body.password,
  });
  const token = createJwtToken({ _id: user._id });
  res.status(201).json({
    status: "success",
    token,
  });
});

exports.protect = catchAsync(async (req, res, next) => {
  let token = req.headers.authorization;
  if (!token || !token.startsWith("Bearer")) {
    return next(new AppError(401, "Please provide a valid token."));
  }
  token = token.split(" ")[1];
  const decoded = jwt.verify(token, process.env.JWT_SECRET);
  const user = await Users.findById(decoded._id);
  if (!user) {
    return next(new AppError(404, "User doesn't exists! Please login again."));
  }
  req.user = user;
  next();
});

exports.getLoggedInUser = catchAsync(async (req, res) => {
  res.status(200).json({ status: "success", user: req.user });
});
