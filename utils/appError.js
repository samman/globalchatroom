class AppError extends Error {
  constructor(statusCode=400, message="An error has occured!") {
    super(message);
    this.status="error";
    this.statusCode = statusCode;
    this.message = message;
    this.isOperational = true;
    Error.captureStackTrace(this, this.constructor);
  }
}

module.exports = AppError;
