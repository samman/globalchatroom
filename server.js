// Importing
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const Pusher = require("pusher");
const cors = require("cors");
const morgan = require("morgan");
const path = require("path");

const authRoutes = require("./routes/authRoutes");
const messagesRoutes = require("./routes/messagesRoutes");
const usersRoutes = require("./routes/usersRoutes");
const errorHandler = require("./controllers/errorHandler");

// App
const app = express();

// Enviroment Varibales Config
dotenv.config({ path: `${__dirname}/.env` });

// Morgan package for loggings requests
if ((process.env.NODE_ENV === "development")) {
  app.use(morgan("dev"));
}

// Middlewares
app.use(express.json());

app.use(cors());

// DB config
mongoose.connect(process.env.DB_CONNECTION_STRING, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Pusher
const pusher = new Pusher({
  appId: process.env.PUSHER_APP_ID,
  key: process.env.PUSHER_KEY,
  secret: process.env.PUSHER_SECRET,
  cluster: process.env.PUSHER_CLUSTER,
  useTLS: true,
});

const db = mongoose.connection;

db.once("open", () => {
  console.log("Database Connected.");

  const msgCollection = db.collection("messages");

  // Mongoose change stream
  const changeStream = msgCollection.watch();

  changeStream.on("change", async (change) => {
    if (change.operationType === "insert") {
      const messageDetails = change.fullDocument;
      try {
        await pusher.trigger("messages", "inserted", messageDetails);
      } catch (err) {
        console.log(err);
      }
    }
  });
});

// API routes
app.use("/api/v1/messages", messagesRoutes);
app.use("/api/v1/auth", authRoutes);
app.use("/api/v1/users", usersRoutes);

if (process.env.NODE_ENV === "production") {
  app.use("/", express.static(`${__dirname}/public`));
  app.get("*", (req, res) => {
    res.sendFile(
      path.join(__dirname, "public", "index.html")
    );
  });
}

// Use Error Handler
app.use(errorHandler);

// Listener
const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log(`Listening to port ${port}`);
});
