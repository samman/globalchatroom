const router = require("express").Router();
const messagesController = require("../controllers/messagesController");
const authController = require("../controllers/authController");

router.route("/sync").get(authController.protect, messagesController.index);

router.route("/new").post(authController.protect, messagesController.store);

module.exports = router;
