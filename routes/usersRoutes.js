const router = require("express").Router();
const authController = require("../controllers/authController");
const usersController = require("../controllers/usersController");

router.route("/").get(authController.protect, usersController.index);

module.exports = router;
