const router = require("express").Router();
const authController = require("../controllers/authController");

router.route("/signin").post(authController.signin);

router.route("/signup").post(authController.signup);

router.route("/profile").get(authController.protect, authController.getLoggedInUser);

module.exports = router;
