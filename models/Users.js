const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const usersSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, "Username is required"],
    unique: true,
  },
  password: {
    type: String,
    require: [true, "Password is required"],
    select: false,
  },
});

usersSchema.pre(
  "save",
  async function (next) {
    if (this.isModified("password"))
      this.password = await bcrypt.hash(this.password, 12);
    next();
  },
  { versionKey: false }
);

module.exports = mongoose.model("Users", usersSchema);
