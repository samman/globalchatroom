const mongoose = require("mongoose");

const MessagesSchema = new mongoose.Schema(
  {
    message: String,
    sender_id: String,
    sender_username: String,
    timestamp: String,
  },
  { versionKey: false }
);

module.exports = mongoose.model("Messages", MessagesSchema);
